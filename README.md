# THOUGHTWORKS UI TEST #

### Packages Needed ###
* NodeJS
* NPM
* Grunt Cli

### Download Packages ###
* to get grunt cli, run `npm install -g grunt-cli`
* run `npm install`  

### Build Process ###

* Developement Build -  only convertes less files to css -
	command : `grunt build` or `grunt build:dev`

* Production Build -  converts and minifies less to css and minifies in a `dist` directory  -
	command : `grunt build:prod`

### Watch ###
* to watch and build less on change - for Developement -
	command : `grunt watch`

### Server ###
* For Developement Build - [http://localhost:3000](http://localhost:3000) -
	command : `grunt connect` or `grunt server` or `grunt connect:dev`

* For Production Build - [http://localhost:3001](http://localhost:3001) -
	command : `grunt connect:prod`