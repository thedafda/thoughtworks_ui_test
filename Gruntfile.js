module.exports = function(grunt) {

  grunt.initConfig({
    less:{
      dev: {
        files: {
          'app/styles.css': 'app/less/*.less'
        }
      },
      prod: {
        options:{
          compress: true
        },
        files: {
          'dist/styles.css': 'app/less/*.less'
        }
      }
    },
    copy: {
      prodhtml: {
        src: 'app/index.html',
        dest: 'dist/index.html'
      },
      prodimages: {
        files:[
          {
            expand: true,
            cwd: 'app/images/',
            src: ['*'],
            dest: 'dist/images/',
            filter: 'isFile'
          }
        ]
      }
    },
    htmlmin: {
      prod: {
        options: {
          removeComments: true,
          collapseWhitespace: true
        },
        files: {
          'dist/index.html': 'dist/index.html',
        }
      }
    },
    watch: {
      less: {
        files: 'app/**/*.less',
        tasks: ['less']
      },
      html: {
        files: 'app/index.html',
        tasks: ['copy:html']
      },
      images: {
        files: 'app/images/*.*',
        tasks: ['copy:image']
      }
    },
    connect:{
      dev:{
        options:{
          port: 3000,
          base: 'app',
          keepalive: true
        }
      },
      prod:{
        options: {
          port: 3001,
          base: 'dist',
          keepalive: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  
  grunt.registerTask('build:dev', ['less:dev']);
  grunt.registerTask('build:prod', ['copy:prodhtml','copy:prodimages','less:prod','htmlmin']);
  grunt.registerTask('build', ['build:dev']);
  grunt.registerTask('server', ['connect:dev']);
};